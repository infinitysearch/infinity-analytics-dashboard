import pymongo
import datetime as dt
from account import mongodb_endpoint


def get_selected_days(start_date, end_date):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['pages']

    selected_pages = col.find({
        'date': {'$gte': start_date, '$lte': end_date}
    })

    all_dates = []
    page_data = {}
    page_dates = {}

    total_page_views = 0
    for post in selected_pages:
        total_page_views += post['views']
        if post['path'] in page_data:
            page_data[post['path']] += post['views']
        else:
            page_data[post['path']] = post['views']

        page_date = post['date'].strftime("%m/%d/%y")
        if page_date in page_dates:
            page_dates[page_date] += post['views']
        else:
            page_dates[page_date] = post['views']

        if page_date in all_dates:
            continue
        else:
            all_dates.append(page_date)

    col = db['referrers']
    selected_referrers = col.find({
        'date': {'$gte': start_date, '$lte': end_date}
    })

    referrer_data = {}
    referrer_dates = {}

    total_referrer_views = 0
    for post in selected_referrers:
        total_referrer_views += post['views']
        if post['referrer'] in referrer_data:
            referrer_data[post['referrer']] += post['views']
        else:
            referrer_data[post['referrer']] = post['views']

        page_date = post['date'].strftime("%m/%d/%y")
        if page_date in referrer_dates:
            referrer_dates[page_date] += post['views']
        else:
            referrer_dates[page_date] = post['views']

    page_data = {k: v for k, v in sorted(page_data.items(), key=lambda item: item[1], reverse=True)}
    referrer_data = {k: v for k, v in sorted(referrer_data.items(), key=lambda item: item[1], reverse=True)}

    ordered_dates = sorted(all_dates)
    ordered_page_dates = sorted(page_dates.items())
    ordered_referrer_dates = sorted(referrer_dates.items())

    return page_data, referrer_data, total_page_views, total_referrer_views, ordered_dates, ordered_page_dates, ordered_referrer_dates


def get_today():
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['pages']

    date = dt.datetime.now()
    year = date.year
    month = date.month
    day = date.day

    selected_pages = col.find({
        'year': year,
        'month': month,
        'day': day,
    })

    all_dates = []

    page_data = {}
    page_dates = {}

    total_page_views = 0
    for post in selected_pages:
        total_page_views += post['views']
        if post['path'] in page_data:
            page_data[post['path']] += post['views']
        else:
            page_data[post['path']] = post['views']

        page_date = post['date'].strftime("%m/%d/%y")
        if page_date in page_dates:
            page_dates[page_date] += post['views']
        else:
            page_dates[page_date] = post['views']

        if page_date in all_dates:
            continue
        else:
            all_dates.append(page_date)

    col = db['referrers']
    selected_referrers = col.find({
        'year': year,
        'month': month,
        'day': day,
    })

    referrer_data = {}
    referrer_dates = {}

    total_referrer_views = 0
    for post in selected_referrers:
        total_referrer_views += post['views']
        if post['referrer'] in referrer_data:
            referrer_data[post['referrer']] += post['views']
        else:
            referrer_data[post['referrer']] = post['views']

        page_date = post['date'].strftime("%m/%d/%y")
        if page_date in referrer_dates:
            referrer_dates[page_date] += post['views']
        else:
            referrer_dates[page_date] = post['views']

    page_data = {k: v for k, v in sorted(page_data.items(), key=lambda item: item[1], reverse=True)}
    referrer_data = {k: v for k, v in sorted(referrer_data.items(), key=lambda item: item[1], reverse=True)}

    ordered_dates = sorted(all_dates)
    ordered_page_dates = sorted(page_dates.items())
    ordered_referrer_dates = sorted(referrer_dates.items())

    return page_data, referrer_data, total_page_views, total_referrer_views, ordered_dates, ordered_page_dates, ordered_referrer_dates


def get_yesterday():
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['pages']

    date = dt.datetime.now() - dt.timedelta(days=1)
    year = date.year
    month = date.month
    day = date.day

    selected_pages = col.find({
        'year': year,
        'month': month,
        'day': day,
    })

    all_dates = []

    page_data = {}
    page_dates = {}

    total_page_views = 0
    for post in selected_pages:
        total_page_views += post['views']
        if post['path'] in page_data:
            page_data[post['path']] += post['views']
        else:
            page_data[post['path']] = post['views']

        page_date = post['date'].strftime("%m/%d/%y")
        if page_date in page_dates:
            page_dates[page_date] += post['views']
        else:
            page_dates[page_date] = post['views']

        if page_date in all_dates:
            continue
        else:
            all_dates.append(page_date)

    col = db['referrers']
    selected_referrers = col.find({
        'year': year,
        'month': month,
        'day': day,
    })

    referrer_data = {}
    referrer_dates = {}

    total_referrer_views = 0
    for post in selected_referrers:
        total_referrer_views += post['views']
        if post['referrer'] in referrer_data:
            referrer_data[post['referrer']] += post['views']
        else:
            referrer_data[post['referrer']] = post['views']

        page_date = post['date'].strftime("%m/%d/%y")
        if page_date in referrer_dates:
            referrer_dates[page_date] += post['views']
        else:
            referrer_dates[page_date] = post['views']

    page_data = {k: v for k, v in sorted(page_data.items(), key=lambda item: item[1], reverse=True)}
    referrer_data = {k: v for k, v in sorted(referrer_data.items(), key=lambda item: item[1], reverse=True)}

    ordered_dates = sorted(all_dates)
    ordered_page_dates = sorted(page_dates.items())
    ordered_referrer_dates = sorted(referrer_dates.items())

    return page_data, referrer_data, total_page_views, total_referrer_views, ordered_dates, ordered_page_dates, ordered_referrer_dates


def get_last_x_days(days):
    start_date = dt.datetime.now() - dt.timedelta(days=days)
    end_date = dt.datetime.now()
    return get_selected_days(start_date, end_date)
