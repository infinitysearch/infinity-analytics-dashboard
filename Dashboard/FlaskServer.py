from flask import Flask, render_template, request, make_response, redirect, url_for, send_from_directory
import os
import datetime as dt

import InfinityAnalyticsDashboard as InfinityAnalyticsDashboard
import AccountManager
from account import website_name

app = Flask(__name__)

if website_name.endswith('/'):
    website_name = website_name[:-1]

@app.route('/', methods=['GET', 'POST'])
def render_home():
    if AccountManager.user_is_logged_in(request) is False:
        return redirect('/login')

    if request.method == 'POST':
        post_data = dict(request.form)

        start_date = post_data['start_date']
        end_date = post_data['end_date']

        start_dt = dt.datetime.strptime(start_date, '%Y-%m-%d')
        end_dt = dt.datetime.strptime(end_date, '%Y-%m-%d')

        data = InfinityAnalyticsDashboard.get_selected_days(start_dt, end_dt)

        pages = data[0]
        referrers = data[1]
        total_page_views = data[2]
        total_referrer_views = data[3]
        ordered_dates = data[4]
        page_chart_data = data[5]
        referrer_chart_data = data[6]

        start_date = start_dt.strftime('%B %d %Y')
        end_date = end_dt.strftime('%B %d %Y')

        return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                               total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                               page_chart_data=page_chart_data,
                               referrer_chart_data=referrer_chart_data, current=start_date + ' - ' + end_date, website_name=website_name)

    if request.args.get('d') is not None:
        date = request.args.get('d')

        if date == 'today':
            data = InfinityAnalyticsDashboard.get_today()
            pages = data[0]
            referrers = data[1]
            total_page_views = data[2]
            total_referrer_views = data[3]
            ordered_dates = data[4]
            page_chart_data = data[5]
            referrer_chart_data = data[6]

            return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                                   total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                                   page_chart_data=page_chart_data,
                                   referrer_chart_data=referrer_chart_data, current="Today", website_name=website_name)

        if date == 'yesterday':
            data = InfinityAnalyticsDashboard.get_yesterday()
            pages = data[0]
            referrers = data[1]
            total_page_views = data[2]
            total_referrer_views = data[3]
            ordered_dates = data[4]
            page_chart_data = data[5]
            referrer_chart_data = data[6]

            return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                                   total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                                   page_chart_data=page_chart_data,
                                   referrer_chart_data=referrer_chart_data, current="Yesterday", website_name=website_name)

        if date == 'seven_days':
            data = InfinityAnalyticsDashboard.get_selected_days(dt.datetime.now() - dt.timedelta(days=7), dt.datetime.now())
            pages = data[0]
            referrers = data[1]
            total_page_views = data[2]
            total_referrer_views = data[3]
            ordered_dates = data[4]
            page_chart_data = data[5]
            referrer_chart_data = data[6]

            return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                                   total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                                   page_chart_data=page_chart_data,
                                   referrer_chart_data=referrer_chart_data, current="Last 7 Days", website_name=website_name)

        if date == 'thirty_days':
            data = InfinityAnalyticsDashboard.get_selected_days(dt.datetime.now() - dt.timedelta(days=30),
                                                            dt.datetime.now())
            pages = data[0]
            referrers = data[1]
            total_page_views = data[2]
            total_referrer_views = data[3]
            ordered_dates = data[4]
            page_chart_data = data[5]
            referrer_chart_data = data[6]

            return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                                   total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                                   page_chart_data=page_chart_data,
                                   referrer_chart_data=referrer_chart_data, current="Last 30 Days", website_name=website_name)

    data = InfinityAnalyticsDashboard.get_selected_days(dt.datetime.now() - dt.timedelta(days=7), dt.datetime.now())

    pages = data[0]
    referrers = data[1]
    total_page_views = data[2]
    total_referrer_views = data[3]
    ordered_dates = data[4]
    page_chart_data = data[5]
    referrer_chart_data = data[6]

    return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                           total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                           page_chart_data=page_chart_data,
                           referrer_chart_data=referrer_chart_data, current="Last 7 Days", website_name=website_name)


@app.route('/custom_days_back', methods=['POST'])
def render_custom_days():
    if AccountManager.user_is_logged_in(request) is False:
        return redirect('/login')

    try:
        post_data = dict(request.form)
        days_back = post_data['last_x_days']

        data = InfinityAnalyticsDashboard.get_selected_days(dt.datetime.now() - dt.timedelta(days=int(days_back)),
                                                        dt.datetime.now())

        pages = data[0]
        referrers = data[1]
        total_page_views = data[2]
        total_referrer_views = data[3]
        ordered_dates = data[4]
        page_chart_data = data[5]
        referrer_chart_data = data[6]

        return render_template('home.html', pages=pages, referrers=referrers, total_page_views=total_page_views,
                               total_referrer_views=total_referrer_views, ordered_dates=ordered_dates,
                               page_chart_data=page_chart_data,
                               referrer_chart_data=referrer_chart_data, current="Last " + str(days_back) + " Days", website_name=website_name)

    except Exception:
        return redirect('/')


@app.route('/login', methods=['GET', 'POST'])
def render_login():
    if AccountManager.user_is_logged_in(request) is True:
        return redirect('/')

    if request.method == 'POST':
        post_data = dict(request.form)
        login = AccountManager.login(post_data['username'], post_data['password'])

        if login != '':
            resp = make_response(redirect('/'))
            resp.set_cookie('InfinityAnalyticsCookie', str(login))
            return resp

        return redirect('/login')

    return render_template('login.html')


@app.route('/logout')
def render_logout():
    if AccountManager.user_is_logged_in(request) is False:
        return redirect('/login')

    # Logout of the page
    resp = make_response(redirect('/'))
    resp.set_cookie('InfinityAnalyticsCookie', '', expires=0)

    return resp


@app.route('/about')
def render_about():
    return render_template('about.html')


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),'favicon.ico', mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run(debug=False)
