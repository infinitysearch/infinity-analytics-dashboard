import pymongo
import hashlib, binascii, os
import uuid
from account import mongodb_endpoint


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


def user_is_logged_in(request):
    cookie = request.cookies.get('InfinityAnalyticsCookie')
    if cookie is None:
        return False

    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['cookies']

    cookie = col.find_one({'cookie': cookie})

    if cookie is None:  # If the cookie does not exist
        return False

    return True


def login(username, password):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['accounts']

    account = col.find_one({'username': username})

    if account is None:  # If the username and password do not match
        return ''

    if verify_password(account['password_hash'], password) is False:
        return ''

    col = db['cookies']
    cookie = col.find_one({'username': username})
    unique_id = str(uuid.uuid4())

    if cookie is None:  # If the cookie does not exist
        col.insert_one({'username': username, 'cookie': unique_id})
        return unique_id

    col.update({'_id': cookie['_id']}, {'username': username, 'cookie': unique_id})

    return unique_id


def logout(request):
    cookie = request.cookies.get('InfinityAnalyticsCookie')
    if cookie is None:
        return True

    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinityAnalytics']
    col = db['cookies']
    col.find_one_and_delete({'cookie': cookie})
    return True
