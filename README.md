# Infinity Analytics Dashboard

#### Important:
This is the repository for viewing your analytics data. If you have not already 
integrated the analytics code into your Flask server, please go to [https://gitlab.com/infinitysearch/infinity-analytics](https://gitlab.com/infinitysearch/infinity-analytics) 
and follow the instructions that are given there. Then, come back to this repository 
and finish the instructions given below. 

## Deployment

### Getting Started
The first thing that you need to do is go to the [account.py](/Dashboard/account.py) file 
and change the mongodb_endpoint variable to your MongoDB endpoint (which can be 
accessed from you MongoDB account online). The other variable that you need to 
change is the website_name variable to your website.

e.g
```python
mongodb_endpoint = 'your_mongodb_endpoint'

# Your website name: an example would be https://infinitysearch.co and not https://infinitysearch.co/home
website_name = ''

```
Make sure that your website_name value is only your domain without any extra paths added to it. 

This is the only code that you need to change. Now, it is time 
to deploy the dashboard. 

### Option 1: Deploying on Heroku 

#### Note:
 This project is already completely setup for you to be able to upload this to a heroku server and
remain in their free tier. It is the easiest and preferred way of deploying the
website. 
<hr/>
All you need to do is create a new heroku project (you 
can name it whatever you want) and follow the heroku instructions for deploying 
the application (only a few command line operations). 

It should be as simple as running these commands below:
```shell script
heroku login
cd my-project/
git init
heroku git:remote -a i-a

git add .
git commit -am "Adding the analytics dashboard"
git push heroku master
```
You should then be able to reach your dashboard at https://your_heroku_project_name.herokuapp.com


### Option 2: Deploying without Heroku
If you would like to host your analytics dashboard on your own, you can do it the 
same way that you would for any other Flask server. The Flask documentation for 
deploying can be found at [https://flask.palletsprojects.com/en/1.1.x/deploying/](https://flask.palletsprojects.com/en/1.1.x/deploying/).

### Option 3: Running locally
To run this locally, you will need to have python3 and the packages in the [requirements.txt](Dashboard/requirements.txt) 
file installed. Once you have python3 installed, make sure that you are in the 
Dashboard directory and then run: 
```shell script
pip3 install -r requirements.txt
``` 

Now, you can view the analytics dashboard by running the wsgi.py file in the 
Dashboard directory and then going to localhost:5000 on your browser.

